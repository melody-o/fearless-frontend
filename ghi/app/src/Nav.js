import React from "react";
import { NavLink } from "react-router-dom";

function Nav() {
    return (
        <div className="container-fluid">
            <NavLink className="nav-link" aria-current="page" to="/locations/new">New location</NavLink>
            <NavLink className="nav-link" aria-current="page" to="/attendees/new">Attend</NavLink>
            <NavLink className="nav-link" aria-current="page" to="/conferences/new">Conference</NavLink>
            <NavLink className="nav-link" aria-current="page" to="/attendees/">Attendees</NavLink>
        </div>
    );
  }

  export default Nav;
