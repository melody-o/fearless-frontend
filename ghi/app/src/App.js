import React from "react";
import AttendeesList from "./AttendeesList";
import Nav from './Nav';
import LocationForm from "./LocationForm";
import AttendConferenceForm from "./AttendConferenceForm";
import ConferenceForm from "./ConferenceForm";
import { BrowserRouter, Routes, Route } from "react-router-dom";

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="locations/new" element={<LocationForm/>} />
          <Route path="attendees/new" element={<AttendConferenceForm/>} />
          <Route path="conferences/new" element={<ConferenceForm/>} />
          <Route path="attendees/" element = {<AttendeesList attendees={props.attendees}/>} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
